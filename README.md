Automation Testing Website Hacktiv8 menggunakan codeceptjs

pada project ini saya membuat sample scenario ada 5.
- Scenario register 
- Scenario masuk ke menu frontend basic
- Scenario masuk ke scholarship
- Scenario masuk ke fullstack immersive
- Scenario masuk ke menu student


mengapa saya menggunakan menggunakan codeceptjs?
- karena menggunakan bahasa javascript yang mudah dimengerti
- konsep codeceptjs yang mudah dimengerti
- tidak memakan memory(ram) yang terlalu besar
- bisa terintegrasi ke web driver,testcafe

untuk menjalankan testcasenya cukup ketik :

    yarn start

Semoga ini bermamfaat.

Terima kasih
