Feature('Menu front end basics');

const url = 'https://hacktiv8.com/';

Scenario('test something', (I) => {
  I.amOnPage(url);
  I.wait(10);
  I.click('#menu2 .container .row .col-lg-5 .bar__module .menu-horizontal .dropdown .dropdown__trigger');
  I.wait(5);
  I.click('.dropdown__content .col-lg-4:nth-child(2) .menu-vertical li:nth-child(2) a');
  I.wait(5);
});
