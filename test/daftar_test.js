Feature('Daftar sekarang');

const url = 'https://hacktiv8.com/';

Scenario('test something', (I) => {
  I.amOnPage(url);
  I.wait(10);
  I.click('.nav-container #menu2 .container .row .col-lg-5:nth-child(3) .bar__module .btn');
  I.wait(5);
  I.scrollTo('#apply .container .row:nth-child(2) .boxed #FSJS');
  I.wait(5);
  I.fillField('#apply .container .row:nth-child(2) .boxed #FSJS .col-md-12:nth-child(1) input[name=name]', 'testing aja');
  I.fillField('#apply .container .row:nth-child(2) .boxed #FSJS .col-md-12:nth-child(2) input[name=lastname]', 'testing aja');
  I.fillField('#apply .container .row:nth-child(2) .boxed #FSJS .col-md-4:nth-child(3) input[name=email]', 'testing@gmail.com');
  I.fillField('#apply .container .row:nth-child(2) .boxed #FSJS .col-md-4:nth-child(4) input[name=birthday]', '7/7/1990');
  I.selectOption('#apply .container .row:nth-child(2) .boxed #FSJS .col-md-4:nth-child(5) select[name=gender]', 'Perempuan');
  I.scrollTo('#apply .container .row:nth-child(2) .boxed #FSJS .col-md-6:nth-child(8)');
  I.executeScript(function () {
    $("#country").val("ae").trigger("change");
  });
  I.appendField('#apply .container .row:nth-child(2) .boxed #FSJS .col-md-12:nth-child(6) textarea[name=address]', 'jalan nurul khasanah');
  I.fillField('#apply .container .row:nth-child(2) .boxed #FSJS .col-md-6:nth-child(8) input[name=city]', 'dki jakarta');
  I.fillField('#apply .container .row:nth-child(2) .boxed #FSJS .col-md-6:nth-child(9) input[name=zip]', '10110');
  I.fillField('#apply .container .row:nth-child(2) .boxed #FSJS .col-md-6:nth-child(10) input[name=phone1]', '8522222');
  I.fillField('#apply .container .row:nth-child(2) .boxed #FSJS .col-md-6:nth-child(11) input[name=phone1conf]', '862222');
  I.fillField('#apply .container .row:nth-child(2) .boxed #FSJS .col-md-6:nth-child(12) input[name=phone2]', '851222222');
  I.fillField('#apply .container .row:nth-child(2) .boxed #FSJS .col-md-6:nth-child(13) input[name=phone2conf]', '8572222');
  I.selectOption('#apply .container .row:nth-child(2) .boxed #FSJS .col-md-6:nth-child(14) select[name=education]', 'Universitas (S1)');
  I.selectOption('#apply .container .row:nth-child(2) .boxed #FSJS .col-md-6:nth-child(15) select[name=how]', 'Linkedin');
  I.selectOption('#apply .container .row:nth-child(2) .boxed #FSJS .col-md-12:nth-child(16) select[name=batch]', 'batchId:FSJS-037,program:FSJS-OFF,order:37,name:(King Fox),prep_start_date:2019-09-16,immersive_start_date:2019-10-28');
  I.selectOption('#apply .container .row:nth-child(2) .boxed #FSJS .col-md-12:nth-child(17) select[name=interview_availability]', 'Sabtu - Minggu (09.00 - 18.00)');
  I.selectOption('#apply .container .row:nth-child(2) .boxed #FSJS .col-md-12:nth-child(18) select[name=direct_immersive]', 'Ya, saya memiliki pengalaman programming dan siap untuk mengambil test tambahan');
  I.appendField('#apply .container .row:nth-child(2) .boxed #FSJS .col-md-12:nth-child(19) textarea[name=education_background]', 'testing');
  I.appendField('#apply .container .row:nth-child(2) .boxed #FSJS .col-md-12:nth-child(20) textarea[name=personality]', 'testing');
  I.appendField('#apply .container .row:nth-child(2) .boxed #FSJS .col-md-12:nth-child(21) textarea[name=why]', 'testing ya');
  I.appendField('#apply .container .row:nth-child(2) .boxed #FSJS .col-md-12:nth-child(22) textarea[name=expectation]', 'testing ya');
  I.fillField('#apply .container .row:nth-child(2) .boxed #FSJS .col-md-12:nth-child(23) input[name=promo_code]', 'promo aja');
  //I.click('#apply .container .row:nth-child(2) .boxed #FSJS .col-md-12:nth-child(7) .input-select span.select2.select2-container');
  //I.click('#FSJS > div:nth-child(7) > div > span');
  I.wait(15);


});